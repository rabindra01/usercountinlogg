package com.logs.count;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserCountInlogs {

	public static void main(String[] args) {
		
		String fileName = "resource/user_logg.txt";
		List<Integer> list = new ArrayList<>();
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			list = stream.map(row ->  row.substring(11,13))
					.map(row -> Integer.parseInt(row))
					.collect(Collectors.toList());
			
			Map<Integer, Long> maprow = list.stream().collect(Collectors.groupingBy(k -> k, Collectors.counting()));
			
			Optional<Entry<Integer, Long>> maxEntry = maprow.entrySet().stream()
					.max((Entry<Integer, Long> e1, Entry<Integer, Long> e2) -> e1.getValue()
							.compareTo(e2.getValue()) );
			    
			System.out.println(" Between "+ maxEntry.get().getKey() + " to "+ (maxEntry.get().getKey().intValue()+1) + " Hours, Total Users count is "+maxEntry.get().getValue());
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		

	}
	

}
