package com.logs.count;

import java.time.Clock;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TimeFunctions {

	public static void main(String[] args) {
		LocalTime startTime = LocalTime.now(ZoneId.systemDefault());
		System.out.println( "startTime-->"+startTime);
		
		LocalTime.now(Clock.systemUTC());
		
		LocalTime endTime= LocalTime.MIDNIGHT.minusHours(10);
		System.out.println( "endTime-->"+endTime);
		timeStearm(startTime, endTime);

	}
	
	static void timeStearm(LocalTime startTime, LocalTime endTime) {
		
		List<Integer> hourList=Stream.iterate(startTime, tm->tm.plusMinutes(1)).limit(ChronoUnit.MINUTES
				.between(startTime, endTime)+60).map(LocalTime::getMinute).collect(Collectors.toList());
		
		//System.out.println( "hourList-->"+hourList.size());
		//System.out.println( "hourList-->"+hourList);
		 Optional<Integer> fDa =hourList.stream().findFirst();
		 if(fDa.isPresent())
			 System.out.println(fDa.get());
	}

}
